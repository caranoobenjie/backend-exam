<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use App\Posts;
use App\Comments;
class Posts extends Model
{
    use HasApiTokens;
    protected $fillable = ['image','title','content','user_id','slug'];

    public function comments()
    {
        return $this->morphMany(Comments::class, 'commentable');
    }

    protected static function boot()
    {
        parent::boot();

        static::created(function ($post) {
            $post->update(['slug' => $post->title]);
        });
    }
}
