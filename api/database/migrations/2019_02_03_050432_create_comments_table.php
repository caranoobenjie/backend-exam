<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('posts_id')->unsigned();
            $table->integer('creator_id')->nullable();
            $table->foreign('posts_id')->references('id')->on('posts');
            $table->text('body')->nullable(); 
            $table->string('creator_type')->nullable(); 
            $table->integer('commentable_id')->unsigned(); 
            $table->string('commentable_type');
            $table->integer('parent_id')->nullable(); 
            $table->integer('_lft')->nullable();
            $table->integer('_rgt')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
