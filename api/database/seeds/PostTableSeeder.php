<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert(array(
            array(
              'title' => 'Migration and Seeder',
              'slug' => 'Laravel',
              'content' => 'message to include the bindings with SQL, which will make this exception a',
              'user_id' => 1
            ),
            array(
              'title' => 'Adding multiple data in seeder',
              'slug' => 'Laravel',
              'content' => 'If an exception occurs when attempting to run a query, well format the error',
              'user_id' => 1
            ),
        ));
        // factory(App\User::class, 10)->create()->each(function ($user) {
        //     $user->posts()->save(factory(App\Post::class)->make());
        // });
    }
}
// $table->increments('id');
// $table->string('title');
// $table->string('slug');
// $table->integer('user_id');