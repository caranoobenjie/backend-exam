import React, { Component } from 'react';

import Button from 'components/Button';

import './AdminForm.scss';

class PostAdminForm extends Component {

  state = {
    title: this.props.post.title,
    content: this.props.post.content
  };

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps.post);
  }

    constructor(props) {
      super(props);
      this.classNamestate = {
        image: null,
      }

      this.onChange = this.onChange.bind(this)
      this.onChange = this.onChange.bind(this)
    }

    onChange(e) {
      const fd = new FormData();
      fd.append('image', this.state.image)
      
      let files = e.target.files || e.dataTransfer.files;
      if (!files.length)
            return;
      this.createImage(files[0]);
    }
    createImage(file) {
      let reader = new FileReader();
      reader.onload = (e) => {
        this.setState({
          image: e.target.result
        })
      };
      reader.readAsDataURL(file);
    }
   


  render() {
    let { onSubmit } = this.props;
    let post = this.state;

    return (
      <form
        className="post-admin-form"
        onSubmit={(e) => { e.preventDefault(); onSubmit(this.state); }}
      >
        <label className="post-admin-form-label">IMAGE</label>
        <input
          className="post-admin-form-text"
          type="file"
          onChange={this.onChange}
        />
        <label className="post-admin-form-label">TITLE</label>
        <input
          className="post-admin-form-text"
          type="text"
          value={post.title}
          onChange={(e) => this.setState({ title: e.target.value })}
        />
        <label className="post-admin-form-label">CONTENT</label>
        <textarea
          rows="10"
          className="post-admin-form-text"
          value={post.content}
          onChange={(e) => this.setState({ content: e.target.value })}
        >
        </textarea>
        <Button>Submit</Button>
      </form>
    );
  }
}

export default PostAdminForm;
